import bannerImg from '../../assets/img/banner-img.png';
export function Banner() {
    return (
        <div className="card bg-dark text-white">
            <img src={bannerImg} className="card-img" alt="banner"/>
        </div>
    );
}