import {Banner} from '../navbar/Banner';
import {Buscador} from '../buscador/Buscador';
import {ListadoPersonaje} from '../personajes/ListadoPersonaje';
import {useState} from 'react';

export function Home() {

    let [buscador, setBuscador] = useState('');

    return (
        <div>
            <Banner/>
            
            <div className="px-5">
                <h1 className="py-5">Personajes destacados</h1>
                <Buscador valor={buscador} onBuscar={setBuscador}/>
                <ListadoPersonaje buscar={buscador}/>
            </div>

            
        </div>
    );
}