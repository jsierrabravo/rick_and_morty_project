import {useParams} from 'react-router-dom';
import {Banner} from '../navbar/Banner';
import axios from 'axios';
import {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import { BannerDetallePersonaje } from '../personajes/BannerDetallePersonaje';
import {ListaEpisodios} from '../episodio/ListaEpisodios'


export function Personaje() {

    let {id} = useParams();
    let [personaje, setPersonaje] = useState(null);
    let [episodios, setEpisodios] = useState(null);

    useEffect(() => {
        axios.get(`https://rickandmortyapi.com/api/character/${id}`)
            .then((response) => {
                setPersonaje(response.data)
            })
    },
    []);

   useEffect(() => {
        if (personaje) {
            let peticionesEpisodios = personaje.episode.map(episode => {
                return axios.get(episode)
            })

            Promise.all(peticionesEpisodios).then((response) => {
                setEpisodios(response)
            })
        }
   }, [personaje]);

    return (
        <div className="p-5">
            {
            personaje ?

            <div>
                <BannerDetallePersonaje {...personaje}/>
                <h2 className='py-5'>Episodios</h2>
                {episodios ? <ListaEpisodios episodios={episodios}/> : <div>Cargando episodios...</div>}
            </div>
            
            : 'Cargando...'}
        </div>
    );
}