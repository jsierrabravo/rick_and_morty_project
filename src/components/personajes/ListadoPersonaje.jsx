import { PersonajeItem } from "./PersonajeItem";
import {useState, useEffect} from 'react';
import axios from 'axios';
export function ListadoPersonaje({buscar}) {

    const [personajes, setPersonajes] = useState(null);
    let personajesFiltrados = personajes?.results;
    console.log(personajesFiltrados);

    if (buscar && personajes ) {
        
        personajesFiltrados = personajes.results.filter((personaje) => {

            let nombrePersonajeMinuscula = personaje.name.toLowerCase()
            let buscadorMinuscula = buscar.toLowerCase()
            return nombrePersonajeMinuscula.includes(buscadorMinuscula);
        })
    }
        

    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character').then(respuesta => {
            setPersonajes(respuesta.data)
        })
    }, []);

    return (
        <div className="row py-5 ml-auto mr-auto">
            {
                personajesFiltrados 
                    ? personajesFiltrados.map((personaje) => {
                        return <PersonajeItem key={personaje.id} {...personaje}/>;
                    })
                    : 'Cargando...'
            }
        </div>
    );
}