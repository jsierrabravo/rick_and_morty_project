export function Buscador({ valor, onBuscar }) {
    return (
        <div className="d-flex justify-content-end">
            <div className="mb-3 col-5">
                <input 
                    type="text" 
                    className="form-control"
                    placeholder="Buscar personaje"
                    value={valor}
                    onChange={(event) => onBuscar(event.target.value)}
                />
            </div>
        </div>
    );
}