import {Episodio} from './Episodio';

export function ListaEpisodios({ episodios }) {
    //console.log(episodios);
    return (
        <div className="row">
            {   
                episodios.map((episodio) => {
                    return <Episodio key={episodio.data.id} {...episodio.data}/>
                })
            }
        </div>
    );
}