import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route, Link } from "react-router-dom";
import {Home} from "./components/pages/Home";
import {Personaje} from "./components/pages/Personaje";
import {Header} from "./components/navbar/Header";
import {Footer} from "./components/footer/Footer";

function App() {

  return (
    <div className="App">

      <Header/>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/personaje/:id" element={<Personaje />} />
      </Routes>

      <Footer/>

    </div>
  )
}

export default App
